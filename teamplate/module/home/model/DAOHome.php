<?php 
$path = $_SERVER['DOCUMENT_ROOT'] . '/1DAW/teamplate/';
include($path . "model/connect.php");


class DAOHome{
    function select_all_home(){
        $sql = "SELECT * FROM reserva ORDER BY name DESC";
        $conexion = connect::con();
        $res = mysqli_query($conexion, $sql);
        connect::close($conexion);

        return $res;

    }
    function select_all_restaurants(){
        $sql= "SELECT nombre,provincia,ciudad,calidad,descripcion FROM restaurantes ORDER BY calidad";
        $connection = connect::con();

        $res = mysqli_query($connection, $sql);
        connect::close($connection);
        return $res;
    }

    function select_restaurant($nombre){

        $sql = "SELECT nombre,provincia,ciudad,calidad,descripcion FROM restaurantes WHERE nombre = '$nombre'";
        $connection = connect::con();

        $res = mysqli_query($connection, $sql)->fetch_object();
        connect::close($connection);
        return $res;
    }


    function select_all_provincia(){
        $sql="SELECT DISTINCT provincia from restaurantes";
        $connection = connect::con();
        $res = mysqli_query($connection, $sql);
        connect::close($connection);
        return $res;
    }

    function select_all_city($provincia){
        $sql="SELECT DISTINCT ciudad from restaurantes WHERE provincia='$provincia'";
        $connection = connect::con();
        $res = mysqli_query($connection, $sql);
        connect::close($connection);
        return $res;
    }
    function select_all_names(){
       
        $tecla_pulsada = $_POST['nombre'];
        $sql = "SELECT * FROM restaurantes WHERE nombre LIKE '" . $tecla_pulsada . "%' GROUP BY 'id'";
       
        $conexion = connect::con();
        $res = mysqli_query($conexion, $sql);
        connect::close($conexion);
        return $res;
    }
}
?>