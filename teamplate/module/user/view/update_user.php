<div id="contenido">
    <form autocomplete="on" method="post" name="update_reserve" id="update_reserve" >
    <?php
		if(isset($error)){
			print ("<BR><span CLASS='styerror'>" . "* ".$error . "</span><br/>");
		}?>
        <h1>Actualizar reserva</h1>
        
        <table border='0'>
            <tr>
                <td>Nombre: </td>
                <td><input type="text" name ="name" id="name" placeholder ="name" value="<?php echo $user['name'];?>"/></td>
                <td><font color="red">
                    <span id="e_name" class="styerror">
                    </span>
                    </font></font></td>
            </tr>

            <tr>
                <td>Apellidos: </td>
                <td><input type="text" name ="surname" id="surname" placeholder ="surname" value="<?php echo $user['surname'];?>"/></td>
                <td><font color="red">
                    <span id="e_surname" class="styerror"></span>
                    </font></font></td>
            </tr>

            <tr>
                <td>Número de personas: </td>
                <td><input type="text" name ="numpeople" id="numpeople" placeholder ="numpeople" value="<?php echo $user['numpeople'];?>"/></td>
                <td><font color="red">
                    <span id="e_numpeople" class="styerror">
                        
                    </span>
                    </font></font></td>
            </tr>

            <tr>
                <td>Email: </td>
                <td><input type="text" name ="email" id="email" placeholder ="email" value="<?php echo $user['email'];?>"/></td>
                <td><font color="red">
                    <span id="e_email" class="styerror">
                        
                    </span>
                    </font></font></td>
            </tr>

            <tr>
                <td>Menu: </td>
                <td><select id="menu" name="menu" placeholder="menu">

                    <?php
                            if($user['menu']=="menu1"){
                        ?>
                    <option value="menu1" selected>Menu1</option>
                    <option value="menu2">Menu2</option>
                    <option value="menu3">Menu3</option>

                    <?php 
                            }elseif($user['menu']=="menu2"){
                    ?>
                    <option value="menu1">Menu1</option>
                    <option value="menu2" selected>Menu2</option>
                    <option value="menu3">Menu3</option>
                    </select></td>

                    <?php
                        }else{
                    ?>

                    <option value="menu1">Menu1</option>
                    <option value="menu2">Menu2</option>
                    <option value="menu3" selected>Menu3</option>
                    <?php
                        }
                    ?>
                <td><font color="red">
                    <span id="e_menu" class="styerror">
                       
                    </span>
                    </font></font></td>
            </tr>
            <tr>
                        <td>Alergenos: </td>
                        <?php
                            $ale=explode(":", $user['alergens']);
                        ?>

                        <td>
                            <?php
                                $search_array=in_array("celiaco",$ale);
                                if($search_array){
                            ?>
                                <input type="checkbox" id= "alergens[]" name="alergens[]" placeholder= "alergens" value="celiaco" checked/>celiaco
                            <?php
                                }else{
                             ?>
                              <input type="checkbox" id= "alergens[]" name="alergens[]" placeholder= "alergens" value="celiaco" />celiaco
                            <?php
                                }
                            ?>
                            <?php
                                $search_array=in_array("carne",$ale);
                                if($search_array){
                            ?>
                               <input type="checkbox" id= "alergens[]" name="alergens[]" placeholder= "alergens" value="carne" checked/>carne
                            <?php
                                }else{
                             ?>
                              <input type="checkbox" id= "alergens[]" name="alergens[]" placeholder= "alergens" value="carne" />carne
                            <?php
                                }
                            ?>
                             <?php
                                $search_array=in_array("verduras",$ale);
                                if($search_array){
                            ?>
                               <input type="checkbox" id= "alergens[]" name="alergens[]" placeholder= "alergens" value="verduras" checked/>verduras
                            <?php
                                }else{
                             ?>
                              <input type="checkbox" id= "alergens[]" name="alergens[]" placeholder= "alergens" value="verduras" />verduras
                            <?php
                                }
                            ?>
                             <?php
                                $search_array=in_array("legumbres",$ale);
                                if($search_array){
                            ?>
                               <input type="checkbox" id= "alergens[]" name="alergens[]" placeholder= "alergens" value="legumbres" checked/>legumbres
                            <?php
                                }else{
                             ?>
                              <input type="checkbox" id= "alergens[]" name="alergens[]" placeholder= "alergens" value="legumbres" />legumbres
                            <?php
                                }
                            ?>
                            <span id="e_alergens" class="styerror">
                        
                        </span>
            </tr>


            <tr>
                <td>Fecha reserva: </td>
                <td><input type="text" name ="reserve_date" id="reserve_date" placeholder ="reserve_date" value="<?php echo $user['reserve_date'];?>"/></td>
                <td><font color="red">
                    <span id="e_reserve_date" class="styerror">
                        
                    </span>
                    </font></font></td>
            </tr>
            <tr>
                <td><input type="button" name="update" id="update" onclick="validate_update()" value="atualitza"/></td>
                <td align="right"><a href="index.php?page=controller_user&op=list">Volver</a></td>
            </tr>
        </table>
    </form>
</div>