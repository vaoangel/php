<?php
session_start();
include("model/controller_functions.php");

if ((isset($_GET['page'])) && ($_GET['page']==="controller_contact")){
	include("view/inc/top_page_contact.php");
}else{
	include("view/inc/top_page.php");
}
if ((isset($_GET['page'])) && ($_GET['page']==="controller_login")){
	include("view/inc/top_page_login.php");
}
    if ((isset($_GET['page'])) && ($_GET['page']==="controller_user") ){
		include("view/inc/top_page_user.php");
	
	}
	
	if ((isset($_GET['page'])) && ($_GET['page']==="controller_order")){
		include("view/inc/top_page_order.php");
	}
	
	if(isset($_GET['module'])){
		include("view/inc/top_page_profile.php");
	}

	 
?>
<div id="wrapper">		
    <div id="header">    	
    	<?php
    	    include("view/inc/header.php");
    	?>  
	
		
    </div>  
    <div id="menu">
		<?php
	
	if(!$_SESSION){
		
		include("view/inc/menu.php");
	}else{
		
		
		switch($_SESSION['type']){
			case 'admin':
			include("view/inc/menu_admin.php");
			break;
			
			case 'user':
			include("view/inc/menu_user.php");
			break;

			default:
			include("view/inc/menu.php");
			break;
		}
	}
		?>
    </div>	
    <div id="">
    	<?php 
			
			if(!isset($_GET['module'])){
				include("view/inc/pages.php");
			}else if((isset($_GET['module'])) && (!isset($_GET['view']))){
				require_once("module/".$_GET['module']."/controller/controller_" .$_GET['module']. ".class.php");
			}
			if ( (isset($_GET['module'])) && (isset($_GET['view'])) ){
				echo "entra isset";
				require_once("module/".$_GET['module']."/view/".$_GET['view'].".html");
			}
				
			
		?>        
        <br style="clear:both;" />
    </div>
    <div id="footer">   	   
	    <?php
	        include("view/inc/footer.php");
	    ?>        
	</div>
	
</div>
<?php
    include("view/inc/bottom_page.php");
?>