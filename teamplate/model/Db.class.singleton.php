<?php
    class Db {
        private $server;
        private $user;
        private $password;
        private $database;
        private $link;
        private $stmt;
        private $array;
        static $_instance;

        private function __construct() {
            $this->setConexion();
            $this->conectar();
        }

        private function setConexion() {
            require_once 'Conf.class.singleton.php';
            $conf = Conf::getInstance();

            $this->server = $conf->_hostdb;
            $this->database = $conf->_db;
            $this->user = $conf->_userdb;
            $this->password = $conf->_passdb;
        }

        private function __clone() {
        }

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }

        private function conectar() {
            $this->link = new mysqli($this->server, $this->user, $this->password);
            $this->link->select_db($this->database);
        }

        public function ejecutar($sql) {
            $this->stmt = $this->link->query($sql);
            return $this->stmt;
        }

        public function listar($sql) {
           
            $this->array = array();
            while ($row = $sql->fetch_array(MYSQLI_ASSOC)) {
                array_push($this->array, $row);
            }
            return $this->array;
             
        //     $sql2 =  $this->link->query($sql)->fetch_object();
        //     debugPHP($sql2);
        //   $prod = array();
		// 		foreach ($sql2 as $row) {
		// 			array_push($prod, $row);
		// 		}
		// 		echo json_encode($prod);
        }

    }
