<?php
 $path = $_SERVER['DOCUMENT_ROOT'] . '/1DAW/teamplate/';
// include($path . "module/login/model/DAOLogin.php");

	$path=$_SERVER['DOCUMENT_ROOT'].'/1DAW/teamplate/';
    if(!defined('SITE_ROOT')) define('SITE_ROOT', $path);
    if(!defined('MODEL_PATH')) define('MODEL_PATH', SITE_ROOT . 'model/');

    class Conf {
        private $_userdb;
        private $_passdb;
        private $_hostdb;
        private $_db;
        static $_instance;

        private function __construct() {
            $cnfg = parse_ini_file(MODEL_PATH."db.ini");
            $this->_userdb = $cnfg['user'];
            $this->_passdb = $cnfg['pass'];
            $this->_hostdb = $cnfg['host'];
            $this->_db = $cnfg['db'];
        }

        private function __clone() {
        }

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }

        public function __get($property) {
            if (property_exists($this, $property)) {
                return $this->$property;
            }
        }
      
    }
