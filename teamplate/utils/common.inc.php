<?php
  function loadModel($model_path, $model_name, $function, $arrArgument = ''){
    $model = $model_path . $model_name . '.class.singleton.php';
//     echo $model_path;
// echo $model_name ;
    if (file_exists($model)) {
        
        include_once($model);
        $modelClass = $model_name;

        if (!method_exists($modelClass, $function)){
            echo "no entra";
            throw new Exception();
        }

        $obj = $modelClass::getInstance();
        if (isset($arrArgument)){
            return $obj->$function($arrArgument);
        }
    } else {
        throw new Exception();
    }
  }

  // /opt/lampp/htdocs/1DAW/teamplate/module/profile/model/model/profile_model.class.singleton.php