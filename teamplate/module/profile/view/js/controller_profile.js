
console.log("in controller");
jQuery.fn.fill_or_clean = function () {
    console.log("in controller");
     this.each(function (){
        //console.log("in controller each");
        if ($("#up_nombre").val() === "") {
            $("#up_nombre").val("Input name ");
            $("#up_nombre").focus(function () {
                if ($("#up_nombre").val() === "Input name ") {
                    $("#up_nombre").val("");
                }
            });
        }
        $("#up_nombre").blur(function () { //Onblur is activated when user changes the focus
            if ($("#up_nombre").val() === "") {
                $("#up_nombre").val("Input nombre");
            }
        });

        if ($("#up_email").val() === "") {
            $("#up_email").val("Input email ");
            $("#up_email").focus(function () {
                if ($("#up_email").val() === "Input email ") {
                    $("#up_email").val("");
                }
            });
        }
        $("#up_email").blur(function () { //Onblur is activated when user changes the focus
            if ($("#up_email").val() === "") {
                $("#up_email").val("Input email");
            }
        });

        if ($("#up_pais").val() === "") {
            $("#up_pais").val("Input pais ");
            $("#up_pais").focus(function () {
                if ($("#up_pais").val() === "Input pais ") {
                    $("#up_pais").val("");
                }
            });
        }
        $("#up_pais").blur(function () { //Onblur is activated when user changes the focus
            if ($("#up_pais").val() === "") {
                $("#up_pais").val("Input pais");
            }
        });

        if ($("#up_provincia").val() === "") {
            $("#up_provincia").val("Input provincia ");
            $("#up_provincia").focus(function () {
                if ($("#up_provincia").val() === "Input provincia ") {
                    $("#up_provincia").val("");
                }
            });
        }
        $("#up_provincia").blur(function () { //Onblur is activated when user changes the focus
            if ($("#up_provincia").val() === "") {
                $("#up_provincia").val("Input provincia");
            }
        });
        

        if ($("#up_ciudad").val() === "") {
            $("#up_ciudad").val("Input ciudad ");
            $("#up_ciudad").focus(function () {
                if ($("#up_ciudad").val() === "Input ciudad ") {
                    $("#up_ciudad").val("");
                }
            });
        }
        $("#up_ciudad").blur(function () { //Onblur is activated when user changes the focus
            if ($("#up_ciudad").val() === "") {
                $("#up_ciudad").val("Input ciudad");
            }
        });
     });
     return this;
};


Dropzone.autoDiscover = false;
$(document).ready(function () {
    
    load_countries_v1();

    $("#cboprovincia").empty();
    $("#cboprovincia").append('<option value="" selected="selected">Select province</option>');
    $("#cboprovincia").prop('disabled', true);
    $("#cboCiudad").empty();
    $("#cboCiudad").append('<option value="" selected="selected">Select city</option>');
    $("#cboCiudad").prop('disabled', true);

    $("#cboPais").change(function() {
		var country = $(this).val();
		var province = $("#cboprovincia");
		var city = $("#cboCiudad");

		if(country !== 'ES'){
	         province.prop('disabled', true);
	         city.prop('disabled', true);
	         $("#cboprovincia").empty();
		     $("#cboCiudad").empty();
		}else{
	         province.prop('disabled', false);
	         city.prop('disabled', false);
	         load_provinces_v1();
		}//fi else
	});
    $("#cboprovincia").change(function() {
		var prov = $(this).val();
		if(prov > 0){
			load_cities_v1(prov);
		}else{
			$("#cboCiudad").prop('disabled', false);
		}
	});

    $('#submit_profile').click(function(){
        console.log("Inside click function");
        //console.log($('input[name="packaging"]:checked').val());
        validate_profile();
    });



    $("#dropzone").dropzone({
        url: "module/profile/controller/controller_profile.class.php?upload=true",
        addRemoveLinks: true,
        maxFileSize: 1000,
        dictResponseError: "An error has occurred on the server",
        acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd',
        init: function () {
            this.on("success", function (file, response) {
                //alert(response);
                $("#progress").show();
                $("#bar").width('100%');
                $("#percent").html('100%');
                $('.msg').text('').removeClass('msg_error');
                $('.msg').text('Success Upload image!!').addClass('msg_ok').animate({'right': '300px'}, 300);
                //console.log(file.name);
                console.log("Response: "+response);
            });
        },
        complete: function (file) {
            //if(file.status == "success"){
            //alert("El archivo se ha subido correctamente: " + file.name);
            //}
        },
        error: function (file) {
            //alert("Error subiendo el archivo " + file.name);
        },
        removedfile: function (file, serverFileName) {
            var name = file.name;
            console.log(name);
            $.ajax({
                type: "POST",
                url: "module/profile/controller/controller_profile.class.php?delete=true",
                data: "filename=" + name,
                success: function (data) {
                  console.log(data);
                    $("#progress").hide();
                    $('.msg').text('').removeClass('msg_ok');
                    $('.msg').text('').removeClass('msg_error');
                    $("#e_avatar").html("");
                    
                    var element2;
                    if ((element2 = file.previewElement) !== null) {
                        element2.parentNode.removeChild(file.previewElement);
                    } else {
                            return false;
                    }
                }
            });
        }
    });//End dropzone

})






function validate_profile(){


    var result = true;

    var name = document.getElementById('up_nombre').value;
    //console.log(name);
    var email = document.getElementById('up_email').value;
    var pais = document.getElementById('cboPais').value;
    var provincia = document.getElementById('cboprovincia').value;
    var ciudad = document.getElementById('cboCiudad').value;
  //  var packaging = $('input[name="packaging"]:checked').val();

    
  var string_reg = /^[0-9a-zA-Z]+[\-'\s]?[0-9a-zA-Z ]+$/;
  var string_mail = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;

  $(".error").remove();
  if ($("#up_nombre").val() === "" || $("#up_nombre").val() === "Input  name"){
    $("#up_nombre").focus().after("<span class='error'>Input  name</span>");
    return false;
  }else if(!string_reg.test($("#up_nombre").val())){
    $("#up_nombre").focus().after("<span class='error'>Name must be 2 to 30 letters</span>");
    return false;
  }

  if ($("#up_email").val() === "" || $("#up_email").val() === "Input an email"){
    $("#up_email").focus().after("<span class='error'>Input an email</span>");
    return false;
  }else if(!string_mail.test($("#up_email").val())){
    $("#up_email").focus().after("<span class='error'>Input a valid email</span>");
    return false;
  }

  if ($("#cboPais").val() === "" || $("#cboPais").val() === "Select a country") {
    $("#cboPais").focus().after("<span class='error'>Select a country</span>");
    return false;
}

if ($("#cboprovincia").val() === "" || $("#cboprovincia").val() === "Select a province") {
    $("#cboprovincia").focus().after("<span class='error'>Select a province</span>");
    return false;
}

if ($("#cboCiudad").val() === "" || $("#cboCiudad").val() === "Select a city") {
    $("#cboCiudad").focus().after("<span class='error'>Select a city</span>");
    return false;
}

if (provincia === null) {
    provincia = 'default_province';
}else if (provincia.length === 0) {
    provincia = 'default_province';
}else if (provincia === 'Select a province') {
    return 'default_province';
}

if (ciudad === null) {
    ciudad = 'default_city';
}else if (ciudad.length === 0) {
    ciudad = 'default_city';
}else if (ciudad === 'Select a city') {
    return 'default_city';
}

var data = {"nombre": name, "email": email, "pais": pais, "provincia": provincia, "ciudad": ciudad};

//console.log(data);
 var data_profile = JSON.stringify(data);
 //console.log(data_profile);

 $.post('module/profile/controller/controller_profile.class.php', {alta_profile:data_profile},
 function (result){
     console.log(result);
     if(result == 'true'){
         alert("Datos actualziados correctamente");
        window.location.href = "index.php?page=controller_home&op=list";
      }else{
          alert('Ha ocurrido un error con la consulta')
          window.location.href = "index.php?module=profile&view=update_profile";
      }
 }
 
 );

 
}


function load_countries_v1(){
   // console.log("entra");
    $.ajax({
        type: "GET",
        url: "module/profile/controller/controller_profile.class.php?load_country=true", 
        success: function(response){
           // console.log(response);
            if(response === 'error'){
                                load_countries_v2("resources/ListOfCountryNamesByName.json");
                            }else{
                                load_countries_v2("module/profile/controller/controller_profile.class.php?load_country=true"); //oorsprong.org
                            }
        }
    })
        .fail(function(response) {
        load_countries_v2("resources/ListOfCountryNamesByName.json");
    });
}

function load_countries_v2(cad) {
    //console.log(cad);
    $.getJSON( cad, function(data) {
      //console.log( data );
      $("#cboPais").empty();
      $("#cboPais").append('<option value="" selected="selected">Select country</option>');

      $.each(data, function (i, valor) {
        $("#cboPais").append("<option value='" + valor.sISOCode + "'>" + valor.sName + "</option>");
      });
    })
    .fail(function() {
        alert( "error load_countries" );
    });
}
function load_provinces_v1(){
  $.ajax({
      type: "GET",
      url: "module/profile/controller/controller_profile.class.php?load_provinces=true",
      success: function(response){
          console.log(response);
            $("#cboprovincia").empty();
            $("#cboprovincia").append('<option value="" selected="selected">Select province</option>');
            var json = JSON.parse(response);
            var provinces=json.provinces;
            if(provinces === 'error'){
                load_provinces_v2();
            }else{
                for (var i = 0; i < provinces.length; i++) {
        		    $("#cboprovincia").append("<option value='" + provinces[i].id + "'>" + provinces[i].nombre + "</option>");
    		    }
            }
      }
  })    
  .fail(function(response) {
    load_provinces_v2();
});
}


// function load_provinces_v1() { //provinciasypoblaciones.xml - xpath
//     $.get( "module/profile/controller/controller_profile.class.php?load_provinces=true",
//         function( response ) {
//           $("#up_provincia").empty();
// 	        $("#up_provincia").append('<option value="" selected="selected">Select province</option>');

//             //alert(response);
//         var json = JSON.parse(response);
// 		    var provinces=json.provinces;
// 		    //alert(provinces);
// 		    //console.log(provinces);

// 		    //alert(provinces[0].id);
// 		    //alert(provinces[0].nombre);

//             if(provinces === 'error'){
//                 load_provinces_v2();
//             }else{
//                 for (var i = 0; i < provinces.length; i++) {
//         		    $("#up_provincia").append("<option value='" + provinces[i].id + "'>" + provinces[i].nombre + "</option>");
//     		    }
//             }
//     })
//     .fail(function(response) {
//         load_provinces_v2();
//     });
// }


function load_provinces_v2() {
    $.get("resources/provinciasypoblaciones.xml", function (xml) {
	    $("#province").empty();
	    $("#province").append('<option value="" selected="selected">Select province</option>');

        $(xml).find("provincia").each(function () {
            var id = $(this).attr('id');
            var name = $(this).find('nombre').text();
            $("#province").append("<option value='" + id + "'>" + name + "</option>");
        });
    })
    .fail(function() {
        alert( "error load_provinces" );
    });
}
function load_cities_v1(prov) { //provinciasypoblaciones.xml - xpath
    var datos = { idPoblac : prov  };
	$.post("module/profile/controller/controller_profile.class.php", datos, function(response) {
	    //alert(response);
        var json = JSON.parse(response);
		var cities=json.cities;
		//alert(poblaciones);
		//console.log(poblaciones);
		//alert(poblaciones[0].poblacion);

		$("#cboCiudad").empty();
	    $("#cboCiudad").append('<option value="" selected="selected">Select city</option>');

        if(cities === 'error'){
            load_cities_v2(prov);
        }else{
            for (var i = 0; i < cities.length; i++) {
        		$("#cboCiudad").append("<option value='" + cities[i].poblacion + "'>" + cities[i].poblacion + "</option>");
    		}
        }
	})
	.fail(function() {
        load_cities_v2(prov);
    });
}
function load_cities_v2(prov) {
    $.get("resources/provinciasypoblaciones.xml", function (xml) {
		$("#cboCiudad").empty();
	    $("#cboCiudad").append('<option value="" selected="selected">Select city</option>');

		$(xml).find('provincia[id=' + prov + ']').each(function(){
    		$(this).find('localidad').each(function(){
    			 $("#cboCiudad").append("<option value='" + $(this).text() + "'>" + $(this).text() + "</option>");
    		});
        });
	})
	.fail(function() {
        alert( "error load_cities" );
    });
}