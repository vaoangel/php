$(document).ready(function () {
    
    // prepare the data
    var source =
    {
        dataType: "json",
        dataFields: [
            { name: 'name', type: 'string' },
            { name: 'surname', type: 'string' },
            { name: 'numpeople', type: 'int' },
            { name: 'email', type: 'string' }
        ],
        id: 'id',
        url: "module/order/controller/controller_order.php?op=datatable"
    };
    
    var dataAdapter = new $.jqx.dataAdapter(source);
    console.log(dataAdapter); 
    $('#dataTable').jqxDataTable(
    {
        width: $("dataTable").width(),
        pageable: true,
        pagerButtonsCount: 10,
        source: dataAdapter,
        sortable: true,
        pageable: true,
        altRows: true,
        filterable: true,
        columnsResize: true,
        pagerMode: 'advanced',
        columns: [
          { text: 'name', dataField: 'name'},
          { text: 'surname', dataField: 'surname' },
          { text: 'numpeople', dataField: 'numpeople' },
          { text: 'email', dataField: 'email'}
      ]
    });  
});
