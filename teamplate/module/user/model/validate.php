<?php
	function validate_reserve(){
		$error='';
		$filtro = array(
			'numpeople' => array(
				'filter'=>FILTER_VALIDATE_REGEXP,
				'options'=>array('regexp'=>'/^.{4,20}$/')
			),
			
			'name' => array(
				'filter'=>FILTER_VALIDATE_REGEXP,
				'options'=>array('regexp'=>'/^\D{3,30}$/')
			),
			
			'surname' => array(
				'filter'=>FILTER_VALIDATE_REGEXP,
				'options'=>array('regexp'=>'/^[a-zA-Z0-9]*$/')
			),
			
			'email' => array(
				'filter'=>FILTER_CALLBACK,
				'options'=>'validatemail'
			),
			
			'menu' => array(
				'filter'=>FILTER_VALIDATE_REGEXP,
				'options'=>array('regexp'=>'/^.{6,12}$/')
			),
			
			'reserve_date' => array(
				'filter' => FILTER_VALIDATE_REGEXP,
				'options' => array('regexp' => '/\d{2}.\d{2}.\d{4}$/')
			)

			
		);
		
		$result=filter_input_array(INPUT_POST,$filtro);
		if(!$result['numpeople']){
			$error='numpeople debe tener de 4 a 20 caracteres';
		}elseif(!$result['name']){
			$error='Nombre debe tener de 3 a 30 caracteres';
		}elseif(!$result['surname']){
			$error='Apellidos debe tener de 4 a 120 caracteres';
		}elseif(!$result['email']){
			$error='El email debe contener de 5 a 50 caracteres y debe ser un email valido';
		}elseif(!$result['reserve_date']){
			$error='Formato fecha dd/mm/yy';	
		}else{
			 return $return=array('resultado'=>true,'error'=>$error,'datos'=>$result);
		};
		return $return=array('resultado'=>false , 'error'=>$error,'datos'=>$result);
	};

	function validatemail($email){
			$email = filter_var($email, FILTER_SANITIZE_EMAIL);
			if(filter_var($email, FILTER_VALIDATE_EMAIL)){
				if(filter_var($email, FILTER_VALIDATE_REGEXP, array('options' => array('regexp'=> '/^.{5,50}$/')))){
					return $email;
				}
			}
			return false;
	}

	
	function debug($array){
		echo "<pre>";
		print_r($array);
		echo "</pre><br>";
	}
?>
