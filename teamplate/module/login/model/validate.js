function validate_register(){
    var email = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;

    if(document.new_register.name.value.length === 0){
        document.getElementById('e_name').innerHTML = "Tienes que escribir el nombre";
        document.new_register.name.focus();
        return 0;
    }
    if(document.new_register.name.value.length < 2){
        document.getElementById('e_name').innerHTML = "El nombre tiene que tener más de 2 caracteres";
        document.new_register.name.focus();
        return 0;
    }

    document.getElementById('e_name').innerHTML= "";

    if(document.new_register.email.value.length === 0){
		document.getElementById('e_email').innerHTML = "Tienes que escribir el mail";
		document.new_register.email.focus();
		return 0;
	}
	if(!email.test(document.new_register.email.value)){
		document.getElementById('e_email').innerHTML = "El formato del mail es invalido";
		document.new_register.email.focus();
		return 0;
	}
    document.getElementById('e_email').innerHTML = "";
    
    if(document.new_register.password.value.length === 0){
        document.getElementById('e_password').innerHTML = "Tienes que escribir la contraseña";
		document.new_register.password.focus();
		return 0;
    }
    if(document.new_register.password.value.length < 6){
		document.getElementById('e_password').innerHTML = "La contraseña tiene que tener más de 6 caracteres";
		document.new_register.password.focus();
		return 0;
    }
    document.getElementById('e_password').innerHTML = "";

    if(document.new_register.password2.value.length === 0){
		document.getElementById('e_password').innerHTML = "Tienes que escribir la contraseña";
		document.formregister.rpassword.focus();
		return 0;
	}
	if(document.new_register.password2.value != document.new_register.password.value){
		document.getElementById('e_password2').innerHTML = "La contraseña tiene que ser la misma";
		document.new_register.rpassword.focus();
		return 0;
	}
	document.getElementById('e_password2').innerHTML = "";
}


function validate_update(){
    var email = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;   

    if(document.new_update.up_email.value.length === 0){
		document.getElementById('error_upemail').innerHTML = "Tienes que escribir el mail";
		document.new_update.email.focus();
		return 0;
	}
	if(!email.test(document.new_update.up_email.value)){
		document.getElementById('error_upemail').innerHTML = "El formato del mail es invalido";
		document.new_update.email.focus();
		return 0;
	}
    document.getElementById('error_upemail').innerHTML = "";

}

function validate_login(){
    var email = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
    if(document.new_login.email.value.length === 0){
		document.getElementById('e_email').innerHTML = "Tienes que escribir el mail";
		document.new_login.email.focus();
		return 0;
	}
	if(!email.test(document.new_login.email.value)){
		document.getElementById('e_email').innerHTML = "El formato del mail es invalido";
		document.new_login.email.focus();
		return 0;
	}
    document.getElementById('e_email').innerHTML = "";


    if(document.new_login.password.value.length === 0){
        document.getElementById('e_password').innerHTML = "Tienes que escribir la contraseña";
		document.new_login.password.focus();
		return 0;
    }
    if(document.new_login.password.value.length < 6){
		document.getElementById('e_password').innerHTML = "La contraseña tiene que tener más de 6 caracteres";
		document.new_login.password.focus();
		return 0;
    }
    document.getElementById('e_password').innerHTML = "";
}



function validate_recover(){
    if(document.new_recover.password.value.length === 0){
        document.getElementById('e_password').innerHTML = "Tienes que escribir la contraseña";
		document.new_recover.password.focus();
		return 0;
    }
    if(document.new_recover.password.value.length < 6){
		document.getElementById('e_password').innerHTML = "La contraseña tiene que tener más de 6 caracteres";
		document.new_recover.password.focus();
		return 0;
    }
    document.getElementById('e_password').innerHTML = "";

    if(document.new_recover.password2.value.length === 0){
		document.getElementById('e_password').innerHTML = "Tienes que escribir la contraseña";
		document.formregister.rpassword.focus();
		return 0;
	}
	if(document.new_recover.password2.value != document.new_recover.password.value){
		document.getElementById('e_password2').innerHTML = "La contraseña tiene que ser la misma";
		document.new_recover.rpassword.focus();
		return 0;
	}
	document.getElementById('e_password2').innerHTML = "";
}

$(document).on('click','.register', function(){
console.log("entra");
    if (validate_register() != 0){
        var data = $("#new_register").serialize();
       $.ajax({
            type: 'POST',
            url: 'module/login/controller/controller_login.php?op=search_user&' +data,
            data:data,
            success: function(response){
                console.log(response);
                if(response=='"noexiste"'){
                    $.ajax({
                        type : 'POST',
                        url : 'module/login/controller/controller_login.php?op=register&' + data,
                        data : data,
                        // beforeSend: function(){	
                        //     console.log(data)
                        //     $("#error_register").fadeOut();
                        // },
                        success: function(response){
            
                            console.log(response);
                             if (response=='"ok"'){
                                 alert("Usuario registrado corerectamente")
                                window.location.href = "index.php?page=controller_login&op=view";
                             };
                        },
                    });
                }else{
                    alert("El usuario ya existe");
                }
            }
       })
      
    };
});
$(document).on('click','.recover', function(){
    console.log("entra recover");
    var data = $("#new_recover").serialize();
    console.log(data);
    $.ajax({

            type :'POST',
            url : 'module/login/controller/controller_login.php?op=recover_all&' +data,
            success: function(data){
            console.log(data);
            if (data=='"ok"'){
                alert("Contraseña actualizada correctamente");
               window.location.href = "index.php?page=controller_home&op=list";
            };
            }
    })
      
    });

$(document).on('click','.login', function(){
    console.log("entra login");
    if (validate_login() != 0){
        var data = $("#new_login").serialize();
        console.log(data);
        $.ajax({
            type: 'POST',
            url: 'module/login/controller/controller_login.php?op=login&' + data,
            data: data,

            success: function(response){
                console.log(response);
                if(response=='"existe"'){
                    alert("Sesion iniciada correctamente");
                    window.location.href = "index.php?page=controller_home&op=list";
                }else{
                    alert("Hay un problema en el inicio se sesión, email o contraseña no válidos")
                }
            }
        })
    };
})

