<!-- container_shop -->
<div id="container_shop">
	<h1>Nuestro catalogo</h1>
	<!-- list_products -->
	<div id="list_products"></div>
</div>

<!-- Modal -->
<!-- exampleModalLabel -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <!-- exampleModalLabel -->
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!-- modal_products -->
        <div id="modal_products"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary addmodal" onclick="InsertCarr()" data-dismiss="modal">Añadir al carrito</button>
      </div>
    </div>
  </div>
</div>

<div class="provamodal"></div>