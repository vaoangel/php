<?php

function validate_profile($value){

    $error = array();
    $valid = true;

    //debugPHP($value,"valprofile");

    $filter = array(
        'nombre' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[0-9A-Za-z\s]{2,30}$/')
        )
        
    );

    $result = filter_var_array($value,$filter);

    //debugPHP($result);

    $result['email'] = $value['email'];
    $result['pais'] = $value['pais'];
    $result['provincia'] = $value['provincia'];
    $result['ciudad'] = $value['ciudad'];
    


    if($result['nombre']==='Input  name'){
        $error['nombre']="Name must be 2 to 30 letters";
        $valid = false;
}
    if ($result['pais']==='Select a country'){
        $error['pais']="You need to choose a country";
        $valid = false;
    }

if ($result['provincia']==='Select a province'){
        $error['provincia']="You need to choose a province";
        $valid = false;
    }

if ($result['ciudad']==='Select a city'){
        $error['ciudad']="You need to choose a city";
        $valid = false;
    }

    return $return = array('result' => $valid, 'error' => $error, 'data' => $result );
}