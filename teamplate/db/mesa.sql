-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Temps de generació: 11-03-2019 a les 19:30:19
-- Versió del servidor: 10.1.37-MariaDB
-- Versió de PHP: 5.6.39

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de dades: `mesa`
--

-- --------------------------------------------------------

--
-- Estructura de la taula `product`
--

CREATE TABLE `product` (
  `nombre` text NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Bolcament de dades per a la taula `product`
--

INSERT INTO `product` (`nombre`, `cantidad`, `precio`) VALUES
('Miranda2', 22, 0),
('miranda', 1, 5),
('Miranda5', 6, 1),
('Miranda2', 22, 0),
('miranda', 1, 5),
('Miranda5', 6, 1),
('Miranda2', 22, 0),
('miranda', 1, 5),
('Miranda5', 6, 1),
('Miranda2', 22, 0),
('miranda', 1, 5),
('Miranda5', 6, 1),
('Miranda2', 22, 0),
('miranda', 1, 5),
('Miranda5', 6, 1),
('Miranda2', 22, 0),
('miranda', 1, 5),
('Miranda5', 6, 1),
('Miranda2', 22, 0),
('miranda', 1, 5),
('Miranda5', 6, 1),
('Miranda2', 22, 0),
('miranda', 1, 5),
('Miranda5', 6, 1);

-- --------------------------------------------------------

--
-- Estructura de la taula `reserva`
--

CREATE TABLE `reserva` (
  `id` int(11) NOT NULL,
  `name` varchar(60) DEFAULT NULL,
  `surname` varchar(60) DEFAULT NULL,
  `numpeople` int(11) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `menu` varchar(200) DEFAULT NULL,
  `reserve_date` varchar(12) DEFAULT NULL,
  `alergens` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Bolcament de dades per a la taula `reserva`
--

INSERT INTO `reserva` (`id`, `name`, `surname`, `numpeople`, `email`, `menu`, `reserve_date`, `alergens`) VALUES
(78, 'asdasd', 'vano', 212121, 'vaoangel@gmail.com', 'menu1', '14/01/2019', 'carne:verduras:'),
(79, 'asdasdee', 'asdasd', 2121, 'vaoangel@gmail.com', 'menu1', '28/01/2019', 'celiaco:carne:verduras:legumbres:');

-- --------------------------------------------------------

--
-- Estructura de la taula `restaurantes`
--

CREATE TABLE `restaurantes` (
  `nombre` text NOT NULL,
  `provincia` text NOT NULL,
  `ciudad` text NOT NULL,
  `calidad` int(11) NOT NULL,
  `descripcion` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Bolcament de dades per a la taula `restaurantes`
--

INSERT INTO `restaurantes` (`nombre`, `provincia`, `ciudad`, `calidad`, `descripcion`) VALUES
('miranda', 'valencia', 'Ontinyent', 5, ''),
('miranda', 'valencia', 'Ontinyent', 5, ''),
('Sifo', 'valencia', 'Bocairent', 8, ''),
('Miranda2', 'Valencia', 'Bocairent', 0, 'asasdasdasdasdasdasdasdasdasdasdasd'),
('Miranda2', 'Valencia', 'Bocairent', 1, 'aaaasdasd'),
('Miranda3', 'Valencia', 'Bocairent2', 1, 'aaa'),
('Miranda4', 'Valencia', 'Bocairent3', 1, 'dddd'),
('Miranda5', 'Valencia', 'Bocairent4', 1, 'ttttt'),
('Miranda6', 'Valencia', 'Bocairent5', 1, 'rrrrr'),
('pepe', 'Alacant', 'Alcoy', 7, 'asdasdasdasdasdasdasd'),
('pepe', 'Alacant', 'Alcoy', 7, 'asdasdasdasdasdasdasd');

-- --------------------------------------------------------

--
-- Estructura de la taula `usuari`
--

CREATE TABLE `usuari` (
  `nombre` text NOT NULL,
  `email` text NOT NULL,
  `password` varchar(255) NOT NULL,
  `avatar` varchar(100) NOT NULL,
  `tipo` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Bolcament de dades per a la taula `usuari`
--

INSERT INTO `usuari` (`nombre`, `email`, `password`, `avatar`, `tipo`) VALUES
('pepe', 'pepe@gmail.com', '1234456', 'asddddddddddddddddddddddddddd', NULL),
('asdasd', 'vaoangel@gmail.com', '123456', '', NULL),
('pepe', 'pepe@gmail.com', '1234456', 'asddddddddddddddddddddddddddd', NULL),
('asdasd2', 'vaoangel@gmail.com', '123456', '', NULL),
('paco', 'paco@gmail.com', '123456', '', NULL),
('antonio', 'antonio@gmail.com', '123445', '', NULL),
('admin', 'admin@gmail', '123456', 'adssssssssssssssssssssssssssssssssss', 'admin'),
('admin1', 'admin1@gmail.com', '123456', '12123123dassssssssssssssssssssss', 'admin'),
('pacoantonio', 'pacopepa@gmail.com', '$2y$18$.GGL3bUM2NCxjAr7QzY5sONsGL/6.XaoXoDATXsPgkyKlenB06W1G\n', '', NULL),
('hash1', 'hash1@gmail.com', '$2y$18$RwuGcEQPYwrbu39Tf2GnUOQi.PdYdXue1kIUaywYJK4M.J1GUIK1y', '', NULL),
('tester3', 'tester@gmail.com', '$2y$12$aoEgjfCWTuadBLT.rDvKzeIn0ZX4KLRaSDFWW3E0ZqUMxnMvopcxa', '', NULL),
('test4', 'vaoan2gel@gmail.com', '$2y$12$lE0eZOmKBTeGY4mtSVpIYulKXDU/ttriOXPX9Joc/iAZ1HUjPI46O', '', NULL),
('asdasdasd', 'asdasd@gmail.com', '$2y$12$yq2QW4u/fXQBxOebEGrAJe1g8Cl.uHCQ2Bl0KiSiMzwxCuZRnZWYm', '', NULL),
('asdasdasd1', 'asdas1d@gmail.com', '$2y$12$YFRJHjS13l9W6Lg0ky5zM..bEMdICh3QaAaFuDq8CZbhRu7LJpKuK', '', NULL),
('asdasasdasd', 'asdasdasdasdasd@gmail.com', '$2y$11$q5MkhSBtlsJcNEVsYh64a.aCluzHnGog7TQAKVmQwO9C8xb.t89F.', 'asddddddddddddddddddddddddd', 'user'),
('administrador', 'admin11@gmail.com', '$2y$12$iC9WN7XDC2QY9Mb4mWeJs.iUvOgqv6PuB7huphuLueS/T.zovhsLe', '', NULL),
('admin22', 'admin22@gmail.com', '$2y$12$VUsa8lZzSxOdIo/Ah8Dyx.yn3TOj/zWfssCe4MEfdKYr1JRpXqinq', '', 'admin'),
('avatar', 'avatar@gmail.com', '$2y$12$7MDRpTIYgp/XDFsQ59tvdeuErMh3JuqVSFSF40TosoSNe93IIAmva', 'https://api.adorable.io/avatars/285/avatar@gmail.com', 'admin'),
('avatar1', 'avatar1@gmail.com', '$2y$12$7.tW995zffiRcddFc0Ti2uRthLue35N/4IS81RWMA17ZfbXd0S.We', 'https://api.adorable.io/avatars/80/avatar1@gmail.com', 'admin');

--
-- Índexs per a les taules bolcades
--

--
-- Índexs per a la taula `reserva`
--
ALTER TABLE `reserva`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT per les taules bolcades
--

--
-- AUTO_INCREMENT per la taula `reserva`
--
ALTER TABLE `reserva`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
